
### Chart
#
#  VERSION 1.1 == CURRENT
# > class created (not finished)
# > Chart displayer created (commented out)
#
#  VERSION 1.0 
# > creation
#
# walk holds item_req, action_req
# plan to make action class which Chart can hold in a dictionary called action


# no touch pls

from walk import *

class Chart(object):
    
  def __init__(self, walk, desc = "", icon = " "):
    self.walk = walk

  @property
  def walk(self):
    return self.__walk
  
  @walk.setter
  def walk(self, newWalk):
    if (type(newWalk) != type(dict())):
      raise TypeError("walk must be a dict")
    else:
      for key in newWalk.keys():
        if (key not in ('item_req','action_req')):
          raise ValueError("invalid key '%s'"% key)
      self.__walk = newWalk
  
chart = [

]

"""
chartHeight  = len(chart)
chartWidth = len(chart[0])

for height in range(-1, chartHeight+1):
  print()
  for width in range(-1, chartWidth+1):
    if (width == -1 or height == -1 or width == chartWidth or height == chartHeight):
      print('#',end="")
    else:
      print(chart[height][width]["icon"],end="")
"""
