from direction import *
class Walk(Direction):
  
  def __init__(self, newItemReq = None, newActionReq = None):
    if newItemReq is not None:
      self.item = newItemReq
    if newActionReq is not None:
      self.action = newActionReq

  def vDir(self, newWalk):
    for key in newWalk.keys():
      if type(newWalk[key]) is not tuple:
        print(newWalk[key], " ", type(newWalk[key]))
        raise TypeError("direction values should be tuples")
      if key not in self.dirList:
        raise ValueError("invalid key or necessary: ('a','n','e','s','w')")
      
  @property
  def item(self):
    return self.__item

  @item.setter
  def item(self, newItemReq):
    if not self.vDir(newItemReq):
      self.__item = newItemReq

  @property
  def action(self):
    return self.__action

  @action.setter
  def action(self, newActionReq):
    self.__action = newActionReq

s = Walk({'a':(),'n':(),'e':(),'s':(),'w':()},{'a':(),'n':(),'e':(),'s':(),'w':()})



