
### Items and Inventory
#
#  VERSION 1.0 == CURRENT
# > creation
#


# the Item class should be used to create new items, which should be declared with a relevant key name
# within the inv dict variable. Being initalized Item requires a name and a description, as well as optionally
# if the user has the item in their inventory (option is there but it should always be False). The
# .gain() and .lose() methods exist as a quicker way to discard a users item.
# A name must be between 1-16 characters, if it is any shorter or longer it will return an error, this is not
# enforced on descriptions as items don't neccessarily require a description.

NAME_MAX = 16
NAME_MIN = 1

class Item(object):
  
  def __init__(self, name, desc, have = False):
    self.name = name
    self.desc = desc
    self.have = have

  @property
  def have(self):
    return self.__have

  @have.setter
  def have(self, newHave):
    if (type(newHave) != type(True)):
      raise TypeError("have value may not be %s"%type(newHave))
    else:
      self.__have = newHave

  def gain(self):
    self.__have = True

  def lose(self):
    self.__have = False

  @property
  def name(self):
    return self.__name

  @name.setter
  def name(self, newName):
    if (type(newName) != type(str())):
      raise TypeError("name value may not be %s"%type(newName))
    elif (len(newName) > NAME_MAX or len(newName) < NAME_MIN):
      raise ValueError("item name lengths musn't proceed 16 characters (your length %s)"%len(newName)) 
    else:
      self.__name = newName

  @property
  def desc(self):
    return self.__desc

  @desc.setter
  def desc(self, newDesc):
    if (type(newDesc) != type(str())):
      raise TypeError("name value may not be %s"%type(newDesc))
    else:
      self.__desc = newDesc

# inv is where all items are stored, the key value should be the exact same as the items name, except all
# lower case.

inv = {
        "sword":Item("Sword", "A sword, it shines brightly in your hand.", True),
        "material flame":Item("Material flame", "Solid material fire, it hurts to hold... a lot.", False)
      }

# viewInv returns items from the inv dict, if the item is .have = False, it will not be shown, unless
# viewAll is set to True, in which case all items will be displayed, viewAll should not be touched except for
# debugging purposes.

def viewInv(viewAll = False):
  for item in inv.keys():
    if (inv[item].have or viewAll != False):
      print("{0:18} {1}".format(inv[item].name, inv[item].desc))

