
### vInput (Validate Input)
# vInput is for checking if all user input is valid, if user input is not valid it will return false and print a related statement,
# else it will return a unified action name and the users "action query".
# Within the function all user input will be uppercase and all string returns will be in uppercase.
# inputPrint should be used for inputting text to be printed.
# echoError is used if you don't want to display the error ( ex: "command not recognized").
# for echo error:
#   -True, displays all errors.
#   -NO_ACTION, won't display action related errors.
#   -NO_CMD, won't display command related errors.
#   -False, won't display any error.
#
#  VERSION 1.1 == CURRENT
# > made cmdList a dict
# > added soft error checking and displaying
# > check for no input
#
#  VERSION 1.0
# > creation
#

#In regards to adding new keys to cmdList, for consistency keep them lowercase,
# it is also recommended that a key have more than two values, otherwise it will display weirdly in echoError displays.
#In regards to adding new values to existing in keys cmdList, a singular value cannot hold more than two words.

"""
cmdList = {
           "take": ("GRAB", "PICK UP", "GET", "TAKE"),
           "talk": ("TALK TO", "SPEAK TO"),
           "open": ("OPEN", "UNBAR", "UNLATCH"),
           "walk": ("WALK", "GO"),
           "view": ("VIEW", "EXAMINE", "SEARCH")
          }
def vInput(inputPrint, echoError = True):
  userInput = str(input(inputPrint).upper()).split(' ')
  userInput += "!!!"
  for key in cmdList.keys():
    if (userInput[0] == ""):
      if (echoError and echoError != "NO_ACTION"):
        print("What?")
      return False
    if (userInput[0]+" "+userInput[1] in cmdList[key]): #checks if user input is within the command list
      if(userInput[2] is not '!'): #checks that there is an "action query"
        return (key +" "+ userInput[2]).upper()
      else:
        if(echoError and echoError != "NO_ACTION"):
          print(userInput[0]+" "+userInput[1]+" what/where?")
        return False
    elif (userInput[0] in cmdList[key]): #checks if user input is within the command list
      if(userInput[1] is not '!'): #checks that there is an "action query"
        return (key +" "+ userInput[1]).upper()
      else:
        if(echoError):
          print(userInput[0]+" what/where?")
        return False
  if(echoError and echoError != "NO_CMD"):
    print("Command not recognized")
  return False
"""

class Validate(object):
  cmdList = {
           "take": ("GRAB", "PICK UP", "GET", "TAKE"),
           "talk": ("TALK TO", "SPEAK TO"),
           "open": ("OPEN", "UNBAR", "UNLATCH"),
           "walk": ("WALK", "GO"),
           "view": ("VIEW", "EXAMINE", "SEARCH")
          }
  
  def __init__(self, inputPrint, echoIssue):
    self.input(input(), inputPrint)

  @property
  def input(self):
    return self.__input

  def input(self, newInput, newPrint = None):
    self.__input = newInput
    if (newPrint != None):
      print(newPrint)
      print("yo what's gud boi")
print(Validate.cmdList["take"])


