
### Intro sequence
# To be shown at the start of the game
#
#  VERSION 1.1 == CURRENT
# > editted to include vInput
# > improved formatting
#
#  VERSION 1.0
# > creation
#

from vInput import *

print("a large door door presents itself you.")
while True:
  action = vInput(">","NO_CMD")
  if (action == "OPEN DOOR"):
    print (" the rain halts and the lightening is dulled, the dimly lit castle atrium before darkens as the doors shuts\n" +
           "just as the last inkling of light leaves a glowing portly shaped ghost fissures in front of you\n" +
           " \"well 'ello there my man, what addle brained life choices 'ave you made to visit this fine establisment of gathered ghouls and ghosts?\"\n" +
           "you seems to be missing a CANDLE, lucky for you i know a guy, YO JIM, \" Jim materialises \"this guys not got a CANDLE would you care to lend him yours?\"\n" + 
           " \"uh. I-I guess, DON'T FREAKIN WRECK MY CANDLE YOU GOT THAT?\"")
    break
  else:
    print (" a voice from beyond the door speaks \"pssSSst.. hey buddy, try OPEN DOOR\" more quietly you also here \"hey jim get a load o' this guy \"")

while True:
  action = vInput(">")
  if (action == "TAKE CANDLE"):
    print (" you take the candle, very clever.")
    break
  else:
    print (" you fumble with your words maybe you should try TAKE CANDLE")
