class Direction(object):

  dirList = (('a', "all"),
             ('n', "north"),
             ('e', "east"),
             ('s', "south"),
             ('w', "west"))
  
  def __init__(self, newWalkDir = None):
    if newWalkDir is not None:
      for key in newWalkDir.keys(): 
        for row in range(0,len(self.dirList)):
          tempList = self.dirList[row]
          if key == tempList[0] or key == tempList[1]:
            exec("self."+tempList[1]+" = newWalkDir[key]")
              
  def verifyInput(self, userInput, propertyName):
    inputType = type(userInput)
    if not inputType is tuple:
      raise TypeError("\"%s\" must be tuple."%propertyName)
    
    
  def appendTo(*addTo, addFrom):
    for item in addFrom:
      if item in addTo:
        print("Warning: item already exists in this directional requirement.")
      else:
        addTo.append(item)
  
  @property
  def all(self):
    return self.__all

  @all.setter
  def all(self, newAll):
    self.verifyInput = newAll
    print("whoops")
    appendTo(self.__all, newAll)

  @property
  def north(self):
    return self.__north

  @north.setter
  def north(self, newNorth):
    self.__north = newNorth

  @property
  def east(self):
    return self.__east

  @east.setter
  def all(self, newEast):
    self.__east = newEast

  @property
  def south(self):
    return self.__south

  @south.setter
  def south(self, newSouth):
    self.__south = newSouth

  @property
  def west(self):
    return self.__west

  @west.setter
  def west(self, newWest):
    self.__west = newWest

d = Direction({'a':('kill','destroy')})
print(d.all)
d.all = {'a':('punch','destroy')}
print(d.all)
d.verifyInput
